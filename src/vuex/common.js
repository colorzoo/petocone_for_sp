import API from '@/libs/api'

const state = {
}

const getters = {
  getData: (state) => (id) => {
    console.log(id)
    return state[id]
  }
}

const mutations = {
  setData(state, {id, data}) {
    state[id] = data
  }
}

const actions = {
  async loadData (context, id) {
    let data = await API.getMasterList(id)
    context.commit('setData', {id, data})
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
}

