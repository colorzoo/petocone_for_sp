import API from '@/libs/api'
import Utils from '@/libs/utils'

const state = {
  badge: 0
}

const getters = {
  topMenu: (state) => {
    return [
      {
        icon: 'home',
        label: 'HOME',
        page: '/index',
        isComponent: true
      },
      {
        icon: 'photo_library',
        label: 'ACTIVITY',
        page: '/photos',
        isComponent: true
      },
      {
        icon: 'photo_camera',
        label: 'POST',
        page: '/camera',
        isComponent: false
      },
      {
        icon: 'notifications',
        label: 'POST',
        page: '/notification',
        badge: state.badge,
        isComponent: true
      },
    ]
  }
}

const mutations = {
  loadingBadge(state, v) {
    state.badge = v
  }
}

const actions = {
  async dispatchLoadBadge ({ commit, dispatch, state }) {
    return await API.getNoticeUnread()
  },
  async loadingBadge ({ commit, dispatch, state }) {
    commit('loadingBadge', await dispatch('dispatchLoadBadge'))
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
}

