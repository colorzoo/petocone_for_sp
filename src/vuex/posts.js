const state = {
  profile: {},
  images: [],
  posts: {
    profile_id: '',
    pics_type: 'img',
    images: [],
    location: ['',''],
    caption: ''
  }
}

const getters = {
  profile: (state) => {
    return state.profile
  },
  postInfo: (state) => {
    return state.posts
  },
  imagesCount: (state) => {
    return Object.keys(state.images).length
  },
  images: (state) => {
    return state.images
  }
}

const mutations = {
  setPostsInfo({ commit }, data) {
    for (var key in data) {
        state.posts[key] = data[key]
    }
  },
  setProfile({ commit }, data) {
    state.profile = data
    state.posts.profile_id = data.profile_id
  },
  clearPostsInfo(state) {
    state.posts = {
      profile_id: '',
      pics_type: 'img',
      images: [],
      location: ['',''],
      caption: ''
    }
    state.images = []
    state.profile = {}
  },
  setImages(state, v) {
    state.images = v
  },
  pushImage(state, v) {
    state.images.push(v)
  }
}

const actions = {
  setPostsInfo(context, data) {
    context.commit('setPostsInfo', data)
  },
  setProfile(context, data) {
    context.commit('setProfile', data)
  },
  clearPostsInfo(context) {
    context.commit('clearPostsInfo')
  },
  setImages(context, data) {
    context.commit('setImages', data)
  },
  pushImages(context, data) {
    context.commit('pushImage', data)
  },
  clearImages(context) {
    context.commit('setImages', [])
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
}

