const state = {
  userInfo: {
    username: '',
    password: '',
    phoneCountry: '+81',
    phoneNumber: '',
    activationkey: ''
  },
  cognito: {
    cognitoUser: ''
  }
}

const getters = {
  userInfo: (state) => {
    return state.userInfo
  },
  phoneNumber: (state) => {
    return `${state.userInfo.phoneCountry}${state.userInfo.phoneNumber}`
  },
  cognitoUser: (state) => {
    return state.cognito.cognitoUser
  }
}

const mutations = {
  setUserInfo(state, val) {
    state.userInfo.userInfo = val
  },
  clearUserInfo(state) {
    state.userInfo.userInfo = {
      username: '',
      password: '',
      phoneCountry: '+81',
      phoneNumber: '',
      activationkey: ''
    }
  },
  setCognitoUser(state, val) {
    state.cognito.cognitoUser = val
  }
}

const actions = {
  setUserInfo(context, data) {
    context.commit('setUserInfo', data)
  },
  clearUserInfo(context) {
    context.commit('clearUserInfo')
  },
  setCognitoUser(context, data) {
    context.commit('setCognitoUser', data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
}

