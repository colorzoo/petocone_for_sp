import API from '@/libs/api'
import Utils from '@/libs/utils'

const state = {
  isLogin: false,
  profile: {
    account: '',
    profile_id: '',
    phone_number: '',
    nickname: '',
    profile: {
      family_name: '',
      first_name: '',
      family_kana: '',
      first_kana: '',
      sex: '',
      birth_year: '',
      birth_mon: '',
      birth_day: '',
      postno: '',
      country: '',
      address: '',
      building: '',
      message: '',
      nickname: ''
    }
  },
  follows: {
    profiles: [],
    family: [],
    friend: [],
    business: []
  }
}

const getters = {
  isLogin: (state) => {
    return state.isLogin
  },
  profile: (state) => {
    return state.profile
  },
  follows: (state) => (type) => {
    return state.follows[type]
  }
}

const mutations = {
  loadingUsers(state, v) {
    if (v){
      state.profile.account = Utils.getValue('account', v)
      state.profile.nickname = Utils.getValue('nickname', v)
      state.profile.phone_number = Utils.getValue('phone_number', v)
      state.profile.profile_id = Utils.getValue('profile_id', v)
      state.profile.profile = Object.assign(state.profile.profile, v.profile)
    }
  },
  loadingFollowers(state, [type, follower]) {
    state.follows[type] = []
    let list = []
    for (var i = 0, len = Object.keys(follower).length; i < len; i++) {
       let user = {}
       user.nickname = follower[i].nickname
       user.owner = follower[i].owner
       user.profile_id = follower[i].profile_id
       user.status = follower[i].status
       user.profile = follower[i].profile
       list.push(user)
    }
    state.follows = { ...state.follows, [type] : list }
  }
}

const actions = {
  async dispatchLoadUsers ({ commit, dispatch, state }) {
    return await API.getUsers()
  },
  async dispatchLoadFollowers ({ commit, dispatch, state }, [type]) {
    let data =await API.getFollows(type)
    return [type, data]
  },
  async loadingUsers ({ commit, dispatch, state }) {
    commit('loadingUsers', await dispatch('dispatchLoadUsers'))
  },
  async loadingFollowers ({ commit, dispatch, state }) {
    commit('loadingFollowers', await dispatch('dispatchLoadFollowers', ['profiles']))
    commit('loadingFollowers', await dispatch('dispatchLoadFollowers', ['family']))
    commit('loadingFollowers', await dispatch('dispatchLoadFollowers', ['friend']))
    commit('loadingFollowers', await dispatch('dispatchLoadFollowers', ['business']))
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
}

