
const state = {
  isLoading: false
}

const getters = {
  isLoading: (state) => {
    return state.isLoading
  }
}

const mutations = {
  loadingSwitch (state) {
    state.isLoading = !state.isLoading
  }
}

const actions = {
  loadingSwitch (context) {
    context.commit('loadingSwitch')
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions
}
