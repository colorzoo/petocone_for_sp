const state = {
  images: [
  ]
}

const getters = {
  imagesCount: (state) => {
    return Object.keys(state.images).length
  },
  images: (state) => {
    return state.images
  },
}

const mutations = {
  setImages(state, v) {
    state.images = v
  },
  pushImage(state, v) {
    state.images.push(v)
  }
}

const actions = {
  setImages(context, data) {
    context.commit('setImages', data)
  },
  pushImages(context, data) {
    context.commit('pushImage', data)
  },
  clearImages(context) {
    context.commit('setImages', [])
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
}

