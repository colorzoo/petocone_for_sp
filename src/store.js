import Vue from 'vue'
import Vuex from 'vuex'

import modLoading from '@/vuex/loading.js'
import modProfiles from '@/vuex/profiles'
import modMenu from '@/vuex/menu'
import modPosts from '@/vuex/posts'
import modImages from '@/vuex/images'

import module_common from '@/vuex/common'
import module_account from '@/vuex/account'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    splitter: {
      namespaced: true,
      state: {
        open: false
      },
      mutations: {
        toggle (state, shouldOpen) {
          if (typeof shouldOpen === 'boolean') {
            state.open = shouldOpen
          } else {
            state.open = !state.open
          }
        }
      }
    },
    modLoading,
    modProfiles,
    modMenu,
    modPosts,
    modImages,
    module_account,
    module_common
  }
})
