import '@mdi/font/css/materialdesignicons.css'
import '@fortawesome/fontawesome-free/css/all.css'
import Vue from 'vue'
import axios from 'axios'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import VueLocalStorage from 'vue-localstorage'
///import './registerServiceWorker'
import VuetifyConfirm from 'vuetify-confirm'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.config.productionTip = false
Vue.use(VueLocalStorage)
Vue.use(VuetifyConfirm, {
  buttonTrueText: 'はい',
  buttonFalseText: 'キャンセル',
  color: 'warning',
  icon: 'warning',
  title: '注意',
  width: 350,
  property: '$confirm'
})

new Vue({
  router,
  store,
  render: h => h(App),
  watch: {
    '$route': function (to, from) {
      // 共通処理を実装する
    },
  },
  localStorage: {
    isBoot: {
      type: Boolean
    }
  }
}).$mount('#app')

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCtTYwz8cJUNAf3vzgT6txJ8Can9FirvBs',
    libraries: 'places'
  }
})
