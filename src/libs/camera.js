const CameraController = {
  getCameraConfig : function () {
    return {
      // 画像の取得元
      sourceType: window.navigator.camera.MediaType.CAMERA,
      // 画質 : 0 〜 100 (高画質)
      quality: 80,
      // 撮影後の写真のサイズ指定
      targetWidth: 640,
      targetHeight: 480,
      // JPG でエンコードする
      encodingType: window.navigator.camera.EncodingType.JPEG,
      // 撮影した画像の編集をさせない
      allowEdit: false,
      // 撮影した画像をどんな形式で受け取るか : Data URL で受け取ることにする
      destinationType: window.navigator.camera.DestinationType.DATA_URL,
    }
  },

  getLibraryConfig : function () {
    return {
//      destinationType: Camera.DestinationType.FILE_URI,
//      sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
//      popoverOptions: new CameraPopoverOptions(300, 300, 100, 100, Camera.PopoverArrowDirection.ARROW_ANY),
//      mediaType: Camera.MediaType.ALLMEDIA
      // 画像の取得元
      sourceType: window.navigator.camera.PictureSourceType.SAVEDPHOTOALBUM,
      // 画質 : 0 〜 100 (高画質)
      quality: 80,
      // 撮影後の写真のサイズ指定
      targetWidth: 640,
      targetHeight: 480,
      // JPG でエンコードする
      encodingType: window.navigator.camera.EncodingType.JPEG,
      // 撮影した画像の編集をさせない
      allowEdit: false,
      // 撮影した画像をどんな形式で受け取るか : Data URL で受け取ることにする
      destinationType: window.navigator.camera.DestinationType.DATA_URL,
//      popoverOptions: new CameraPopoverOptions(300, 300, 100, 100, window.navigator.camera.PopoverArrowDirection.ARROW_ANY),
//      mediaType: window.navigator.camera.MediaType.ALLMEDIA
    }
  },

  captureCamera : function (onSuccess, onError) {
    navigator.camera.getPicture(onSuccess,onError, CameraController.getCameraConfig());
  },

  captureLibrary : function (onSuccess, onError) {
    navigator.camera.getPicture(onSuccess, onError, CameraController.getLibraryConfig());
  }

}

export default CameraController;

