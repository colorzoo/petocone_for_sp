
const Utils = {

  getValue: function (s, obj){
    if (s in obj){
      return obj[s]
    }
    return null
  },

  dataURItoBlob: function (dataURI) {
    var binary = atob(dataURI.split(',')[1])
    var array = []
    for(var i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i))
    }
    return new Blob([new Uint8Array(array)], {type: 'image/jpeg'})
  }
}

export default Utils;

