import Auth from "@/libs/auth"
import Utils from "@/libs/utils"

import axios from "axios"

const Api = {

  /**
   * マスター関連情報の取得
   *
   */
  getMasterList: async function (master, options = '') {
    const res = await axios.get(process.env.VUE_APP_AWS_API_ENDPOINT + "/lists/" + master + ((options != '') ? '/' + options : ''))
    if (res.status == 200){
        return res.data
    }
    throw new Error(res.status)
  },

  /**
   * アカウント存在チェック
   *
   */
  getAccountsAccount: async function (account) {
    let res = await axios.get(process.env.VUE_APP_AWS_API_ENDPOINT + "/accounts/" + account)
    if (res.status == 200){
      if (res.data === true){
        return res.data
      }
    }
    throw new Error(res.status)
  },

  /**
   * 自己アカウント返却
   *
   */
  getUsers: async function () {
    return await axios({
      method: "GET",
      url: process.env.VUE_APP_AWS_API_ENDPOINT + "/users/i/",
      headers: {"Authorization": Auth.getJwtToken() },
      params: {}
    })
      .then(function (response) {
        if (response.status === 200) {
          if (response.data.profile) {
            if (typeof response.data.profile !== 'object') {
              response.data.profile = JSON.parse(response.data.profile)
            }
          }
          return response.data
        } else {
          return false
        }
      })
      .catch(err => {
        return false
      })
  },

  /**
   * 自己アカウント登録
   *
   */
  postUsers: async function (data = {}) {
    return await axios({
        method: "POST",
        url: process.env.VUE_APP_AWS_API_ENDPOINT + "/users/i/",
        headers: {"Authorization": Auth.getJwtToken() },
        data   : data
      })
        .then(function (response) {
          if (response.status === 200) {
            return true
          } else {
            throw new Error(response.status)
          }
        })
        .catch(err => {
          throw new Error(err)
        })
  },

  /**
   * 自己アカウント更新
   *
   */
  putUsers: async function (data = {}) {
    return await axios({
        method: "PUT",
        url: process.env.VUE_APP_AWS_API_ENDPOINT + "/users/i/",
        headers: {"Authorization": Auth.getJwtToken() },
        data   : data
      })
        .then(function (res) {
          if (res.status === 200) {
            return res.data
          } else {
            throw new Error(res.status)
          }
        })
        .catch(err => {
          throw new Error(err)
        })
  },

  /**
   * 自己アカウント削除
   *
   */
  deleteUsers: async function () {
    return await axios({
        method: "DELETE",
        url: process.env.VUE_APP_AWS_API_ENDPOINT + "/users/i/",
        headers: {"Authorization": Auth.getJwtToken() },
      })
        .then(function (res) {
          if (res.status === 200) {
            return true
          } else {
            throw new Error(res.status)
          }
        })
        .catch(err => {
            throw new Error(err)
        })
  },

  /**
   * プロフィール返却（共通）
   *
   */
  getUsersAccount: async function ($profileId = '') {
    return await axios({
        method: "GET",
        url: process.env.VUE_APP_AWS_API_ENDPOINT + "/users/profile/" + $profileId,
        headers: {"Authorization": Auth.getJwtToken() }
      })
        .then(function (res) {
          if (res.status === 200) {
            if (res.data.profile) {
              if (typeof res.data.profile !== 'object') {
                res.data.profile = JSON.parse(res.data.profile)
              }
            }
            return res.data
          } else {
            throw new Error(res.status)
          }
        })
        .catch(err => {
            throw new Error(err)
        })
  },

  /**
   * プロフィール削除（共通）
   *
   */
  deleteUsersAccount: async function ($profileId = '') {
    return await axios({
        method: "DELETE",
        url: process.env.VUE_APP_AWS_API_ENDPOINT + "/users/profile/" + $profileId,
        headers: {"Authorization": Auth.getJwtToken() },
      })
        .then(function (res) {
            if (res.status === 200) {
            return res.data
          } else {
            throw new Error(res.status)
          }
        })
        .catch(err => {
            throw new Error(err)
        })
  },

  /**
   * プロフィール更新（共通）
   *
   */
  putUsersAccount: async function ($profileId = '', data = {}) {
    return await axios({
        method: "PUT",
        url: process.env.VUE_APP_AWS_API_ENDPOINT + "/users/profile/" + $profileId,
        headers: {"Authorization": Auth.getJwtToken() },
        data: data
      })
        .then(function (res) {
            if (res.status === 200) {
            return res.data
          } else {
            throw new Error(res.status)
          }
        })
        .catch(err => {
            throw new Error(err)
        })
  },

  /**
   * アカウント非表示
   *
   */
  postUsersHide: async function (profile_id) {
    return await axios({
        method: "POST",
        url: process.env.VUE_APP_AWS_API_ENDPOINT + "/users/hide/" + profile_id,
        headers: {"Authorization": Auth.getJwtToken() }
      })
      .then(function (res) {
        if (res.status === 200) {
          return true
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
   * アカウント表示
   *
   */
  postUsersShow: async function (profile_id) {
    return await axios({
        method: "POST",
        url: process.env.VUE_APP_AWS_API_ENDPOINT + "/users/show/" + profile_id,
        headers: {"Authorization": Auth.getJwtToken() }
      })
      .then(function (res) {
        if (res.status === 200) {
          return true
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
   * パートナー登録
   *
   */
  postPartners: async function (data = {}) {
    return await axios({
        method: "POST",
        url: process.env.VUE_APP_AWS_API_ENDPOINT + "/users/i/partner/",
        headers: {"Authorization": Auth.getJwtToken() },
        data   : data
      })
      .then(function (res) {
          if (res.status === 200) {
          return res.data
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
   * 友だち一覧
   * $role =
   *   profiles  ･･･ パートナー一覧
   *   family    ･･･ 家族一覧
   *   friend    ･･･ 友達一覧
   *   business  ･･･ ビジネスアカウント一覧
   *
   */
  getFollows: async function ($role = 'profiles') {
    return await axios({
        method: "GET",
        url: process.env.VUE_APP_AWS_API_ENDPOINT + "/follows/ls/" + $role + "/",
        headers: {"Authorization": Auth.getJwtToken() },
      })
      .then(function (res) {
        if (res.status === 200) {
          return res.data
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
   * 友だちのパートナー一覧
   *
   */
  getFollowPartnerList: async function () {
    return await axios({
      method: "GET",
      url: process.env.VUE_APP_AWS_API_ENDPOINT + "/follows/partner/ls/",
      headers: {"Authorization": Auth.getJwtToken() },
    })
      .then(function (res) {
        if (res.status === 200) {
          return res.data
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
   * 友だちのパートナー一覧
   *
   */
  getFollowPartner: async function (profileId) {
    if (profileId == ''){
      throw new Error("NOT FOUND")
    }
    return await axios({
      method: "GET",
      url: process.env.VUE_APP_AWS_API_ENDPOINT + "/follows/partner/ls/" + profileId,
      headers: {"Authorization": Auth.getJwtToken() },
    })
      .then(function (res) {
        if (res.status === 200) {
          return res.data
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
   * 友達削除
   *
   */
  deleteFollowRemove: async function (profileId) {
    return await axios({
      method: "DELETE",
      url: process.env.VUE_APP_AWS_API_ENDPOINT + "/follows/rm/" + profileId,
      headers: {"Authorization": Auth.getJwtToken() },
    })
      .then(function (res) {
        if (res.status === 200) {
          if (res.data == false){
            throw new Error("ERROR")
          }
          return res.data
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
   * 友達非表示
   *
   */
  putFollowHide: async function (profileId) {
    return await axios({
      method: "PUT",
      url: process.env.VUE_APP_AWS_API_ENDPOINT + "/follows/hide/" + profileId,
      headers: {"Authorization": Auth.getJwtToken() },
    })
      .then(function (res) {
        if (res.status === 200) {
          if (res.data == false){
            throw new Error("ERROR")
          }
          return res.data
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
   * 友達表示
   *
   */
  putFollowShow: async function (profileId) {
    return await axios({
      method: "PUT",
      url: process.env.VUE_APP_AWS_API_ENDPOINT + "/follows/show/" + profileId,
      headers: {"Authorization": Auth.getJwtToken() },
    })
      .then(function (res) {
        if (res.status === 200) {
          if (res.data == false){
            throw new Error("ERROR")
          }
          return res.data
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
   * 友達申請一覧返却
   *
   */
  getFollowRequest: async function (roleType) {
    return await axios({
      method: "GET",
      url: process.env.VUE_APP_AWS_API_ENDPOINT + "/follows/request/ls/" + roleType,
      headers: {"Authorization": Auth.getJwtToken() },
    })
      .then(function (res) {
        if (res.status === 200) {
          return res.data
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
   * 友だち承認テキスト返却
   *
   */
  getFollowRequestFriend: async function () {
    return await axios({
      method: "GET",
      url: process.env.VUE_APP_AWS_API_ENDPOINT + "/follows/request/friend",
      headers: {"Authorization": Auth.getJwtToken() }
    })
      .then(function (res) {
        if (res.status === 200) {
          return res.data.text
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
   * 友達申請
   *
   */
  postFollowRequestFriend: async function (data) {
    return await axios({
      method: "POST",
      url: process.env.VUE_APP_AWS_API_ENDPOINT + "/follows/request/friend",
      headers: {"Authorization": Auth.getJwtToken() },
      data   : data
    })
      .then(function (res) {
        if (res.status === 200) {
          return res.data
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
   * 家族申請
   *
   */
  postFollowRequestFamily: async function (profile_id) {
    return await axios({
      method: "POST",
      url: process.env.VUE_APP_AWS_API_ENDPOINT + "/follows/request/family/" + profile_id,
      headers: {"Authorization": Auth.getJwtToken() },
    })
      .then(function (res) {
        if (res.status === 200) {
          return res.data
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
   * 家族解除
   *
   */
  deleteFollowRequestFamily: async function (profile_id) {
    return await axios({
      method: "DELETE",
      url: process.env.VUE_APP_AWS_API_ENDPOINT + "/follows/family/" + profile_id,
      headers: {"Authorization": Auth.getJwtToken() },
    })
      .then(function (res) {
        if (res.status === 200) {
          return res.data
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
   * 友達承認
   *
   */
  putFollowRequest: async function (bondsRequestsId) {
    return await axios({
      method: "PUT",
      url: process.env.VUE_APP_AWS_API_ENDPOINT + "/follows/request/" + bondsRequestsId,
      headers: {"Authorization": Auth.getJwtToken() },
    })
      .then(function (res) {
        if (res.status === 200) {
          if (res.data == false){
            throw new Error("ERROR")
          }
          return res.data
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },


  /**
   * 投稿一覧の取得
   *
   */
  getPosts: async function (postType = '', page, per_page, profileId = '') {
    return await axios({
        method: "GET",
        url: process.env.VUE_APP_AWS_API_ENDPOINT + "/posts/ls/" + ((postType == '') ? '' : postType + '/') + ((profileId == '') ? '' : (profileId + '/')) + page + '/' + per_page,
        headers: {"Authorization": Auth.getJwtToken() }
      })
        .then(function (res) {
          if (res.status === 200) {
            return res.data
          } else {
            throw new Error(res.status)
          }
        })
        .catch(err => {
          throw new Error(err)
        })
  },

  /**
   * プロフィール画像アップロード
   *
   */
  postPicsProfile: async function (profile_id, file) {
    const blob = new Utils.dataURItoBlob(file)
    let formData = new FormData()
    formData.append('images', blob)
    let config = {
      headers: {
        "Authorization": Auth.getJwtToken(),
        'Content-Type': 'multipart/form-data'
      }
    }
    return await axios.post(process.env.VUE_APP_AWS_API_ENDPOINT + "/pics/profile/" + profile_id, formData, config)
      .then(function (res) {
        if (res.status === 200) {
          return res.data
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
   * 投稿画像アップロード
   *
   */
  postPicsPhotos: async function (file) {
    const blob = new Utils.dataURItoBlob(file)
    let formData = new FormData()
    formData.append('images', blob)
    let config = {
      headers: {
        "Authorization": Auth.getJwtToken(),
        'Content-Type': 'multipart/form-data'
      }
    }
    return await axios.post(process.env.VUE_APP_AWS_API_ENDPOINT + "/pics/photos/", formData, config)
      .then(function (res) {
        if (res.status === 200) {
          return res.data
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
   * 投稿登録
   *
   */
  postPosts: async function (data) {
    return await axios({
      method: "POST",
      url: process.env.VUE_APP_AWS_API_ENDPOINT + "/posts/i",
      headers: {"Authorization": Auth.getJwtToken() },
      data   : data
    })
      .then(function (res) {
        if (res.status === 200) {
          return res.data
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
   * 投稿詳細
   *
   */
  getPostsView: async function (postId) {
    return await axios({
      method: "GET",
      url: process.env.VUE_APP_AWS_API_ENDPOINT + "/posts/view/" + postId,
      headers: {"Authorization": Auth.getJwtToken() },
    })
      .then(function (res) {
        if (res.status === 200) {
          return res.data
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },



  /**
   * 会員証返却
   *
   */
  getMemberID: async function () {
    return await axios({
      method: "GET",
      url: process.env.VUE_APP_AWS_API_ENDPOINT + "/members/",
      responseType: 'arraybuffer',
      headers: {"Authorization": Auth.getJwtToken() },
    })
      .then(function (res) {
        if (res.status === 200) {
          const base64Data = Buffer.from(res.data, 'binary').toString('base64');
          const prefix = `data:${res.headers['content-type']};base64,`;
          return `${prefix}${base64Data}`
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
   * 会員証返却(QRコード)
   *
   */
  getMemberQRCode: async function () {
    return await axios({
      method: "GET",
      url: process.env.VUE_APP_AWS_API_ENDPOINT + "/members/qr/",
      responseType: 'arraybuffer',
      headers: {"Authorization": Auth.getJwtToken() },
    })
      .then(function (res) {
        if (res.status === 200) {
          const base64Data = Buffer.from(res.data, 'binary').toString('base64');
          const prefix = `data:${res.headers['content-type']};base64,`;
          return `${prefix}${base64Data}`
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
   * 画像返却
   *
   */
  getPhoto: async function (url) {
    return await axios({
      method: "GET",
      url: process.env.VUE_APP_AWS_API_ENDPOINT + url,
      responseType: 'arraybuffer',
      headers: {"Authorization": Auth.getJwtToken() },
    })
      .then(function (res) {
        if (res.status === 200) {
          const base64Data = Buffer.from(res.data, 'binary').toString('base64');
          const prefix = `data:${res.headers['content-type']};base64,`;
          return `${prefix}${base64Data}`
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

  /**
  * 投稿共有
  *
  */
  putPostsShare: async function (postsId) {
    return await axios({
        method: "PUT",
        url: process.env.VUE_APP_AWS_API_ENDPOINT + "/posts/share/follow/" + postsId,
        headers: {"Authorization": Auth.getJwtToken() }
    })
      .then(function (res) {
          if (res.status === 200) {
              if (res.data == false){
                  throw new Error("ERROR")
              }
              return res.data
          } else {
              throw new Error(res.status)
          }
      })
      .catch(err => {
          throw new Error(err)
      })
  },

  /**
   * 投稿共有解除
   *
   */
  deletePostsShare: async function (postsId) {
    return await axios({
        method: "DELETE",
        url: process.env.VUE_APP_AWS_API_ENDPOINT + "/posts/share/follow/" + postsId,
        headers: {"Authorization": Auth.getJwtToken() },
    })
      .then(function (res) {
          if (res.status === 200) {
              if (res.data == false){
                  throw new Error("ERROR")
              }
              return res.data
          } else {
              throw new Error(res.status)
          }
      })
      .catch(err => {
          throw new Error(err)
      })
  },

  /**
   * 投稿削除
   *
   */
  deletePosts: async function (postsId) {
    return await axios({
        method: "DELETE",
        url: process.env.VUE_APP_AWS_API_ENDPOINT + "/posts/i/" + postsId,
        headers: {"Authorization": Auth.getJwtToken() },
    })
      .then(function (res) {
          if (res.status === 200) {
              if (res.data == false){
                  throw new Error("ERROR")
              }
              return res.data
          } else {
              throw new Error(res.status)
          }
      })
      .catch(err => {
        throw new Error(err)
    })
  },

  /**
   * お知らせ返却
   *
   */
  getNotice: async function (page, per_page) {
    return await axios({
        method: "GET",
        url: process.env.VUE_APP_AWS_API_ENDPOINT + "/notice/" + page + "/" + per_page,
        headers: {"Authorization": Auth.getJwtToken() },
      })
      .then(function (res) {
          if (res.status === 200) {
          return res.data
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
          throw new Error(err)
      })
  },

  /**
   * 未読お知らせ件数
   *
   */
  getNoticeUnread: async function () {
    return await axios({
        method: "GET",
        url: process.env.VUE_APP_AWS_API_ENDPOINT + "/notice/unread",
        headers: {"Authorization": Auth.getJwtToken() },
      })
      .then(function (res) {
          if (res.status === 200) {
          return res.data
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
          throw new Error(err)
      })
  },

  /**
   * お知らせ開封
   *
   */
  putNoticeOpened: async function (notice_id) {
    return await axios({
        method: "PUT",
        url: process.env.VUE_APP_AWS_API_ENDPOINT + "/notice/opened/" + notice_id,
        headers: {"Authorization": Auth.getJwtToken() },
      })
      .then(function (res) {
          if (res.status === 200) {
          return res.data
        } else {
          throw new Error(res.status)
        }
      })
      .catch(err => {
          throw new Error(err)
      })
  },

  getSchedule: async function (profile_id, type, date) {
    return await axios({
        method: "GET",
        url: process.env.VUE_APP_AWS_API_ENDPOINT + '/schedule/' + profile_id + '/' + type + '/' + date,
        headers: {"Authorization": Auth.getJwtToken() },
      })
      .then(function (response) {
        console.log("----------------------")
        console.log(response.status)
        console.log(response.data)
        console.log("----------------------")
        if (response.status === 200) {
          return response.data
        } else {
          throw new Error(response.status)
        }
      })
      .catch(err => {
        throw new Error(err)
      })
  },

}

export default Api;

