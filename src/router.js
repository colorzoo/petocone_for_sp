import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/views/Login.vue'

Vue.use(Router)

const router = new Router({
  mode: process.env.CORDOVA_PLATFORM ? 'hash' : 'history',
//   mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      // path: '/login',
      path: '/',
      name: 'login',
      component: Login,
    }
  ]
});

// router.beforeEach((to, from, next) => {
//   (async () => {
//     console.log(to)
//     console.log(from)
//     if (to.matched.some(record => record.meta.requiresAuth)) {
//       await Auth.currentSession()
//     }
//     next()
//   })().catch((e) => {
//     next({ path: '/login', query: { redirect: to.fullPath }})
//   });
// })
export default router;
