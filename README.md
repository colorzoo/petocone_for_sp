# petocone_for_sp

## gitからclone
``` 
git clone https://akamatsuy@bitbucket.org/colorzoo/petocone_for_sp.git
```

## プロジェクトセットアップ
```
cd petocone_for_sp
npm install
```
## vue-cli
```
npm install -g @vue/cli
```

### cordovaプロジェクトを作成
```
npm install -g cordova
vue add cordova
色々聞かれる
```

### 実機のプラットフォームをインストール
```
cd ./cordova
cordova platform add android
cordova platform add ios

cd ../

```

### ブラウザでとりあえず確認する
```
npm run serve
```

### PCのローカ用にビルド
```
npm run build
```

### Androidで実機確認するときのビルド
```
npm run cordova-build-android
```
### Androidで実機確認する
```
cd ./cordova
cordova run android
```

### iosで実機確認する
```
cd ./cordova
cordova run ios --buildFlag='-UseModernBuildSystem=0'
```



